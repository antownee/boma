var nfc = require('./lib/nfc');
var async = require('async');
var express = require('express')
var app = express();
var portNo = 3000;


nfc.open((err,device)=> {
    if(err){ throw err};
    device.getDeviceFirmwareVersion(function (err, version) {
        console.log('Version:', version); // Version: ACR122U213
    })

    device.setCardBuzzer(true,(err,res)=>{
        console.log("Buzzer set")
    })

    device.setPICC(true,(err,res)=>{
        console.log("PICC set")
    })
})

app.listen(portNo, ()=> {
    console.log("Check in app listening on port " + portNo)
})